﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour
{
    public Transform block1;
    public Transform block2;
    public Transform block3;
    public Transform block4;
    public Transform block5;
    public Transform block6;
    public Transform block7;

    private Transform thisBlock;
    private bool pressed = false;

    public Transform TContainer;
    public static bool ready = true;

    public int numOfBlocks;
    private Transform[] Blocks;

    private float x;
    private float y;
    private float z;

    public BoxCollider2D gameover; 
    // Use this for initialization
    void Start()
    {
        Blocks = new Transform[numOfBlocks];
        ready = true;
    }

    void Update()
    {

        if ((ready) || ((Input.GetKeyDown(KeyCode.Space)) && (!pressed)))
        {
            pressed = true;
            int c = Random.Range(1, 7);

            switch (c)
            {
                case 1:
                    thisBlock = block1;
                    x = 0;
                    y = 0.07f;
                    z = 0;
                    break;

                case 2:
                    thisBlock = block2;
                    x = 0;
                    y = 0.07f;
                    z = 0;
                    break;

                case 3:
                    thisBlock = block3;
                    x = 0;
                    y = 0.07f;
                    z = 0;
                    break;

                case 4:
                    thisBlock = block4;
                    x = 0;
                    y = 0;
                    z = 0;
                    break;

                case 5:
                    thisBlock = block5;
                    x = 0.07f;
                    y = 0;
                    z = 0;
                    break;

                case 6:
                    thisBlock = block6;
                    x = 0;
                    y = 0.07f;
                    z = 0;
                    break;

                case 7:
                    thisBlock = block7;
                    x = 0;
                    y = 0.7f;
                    z = 0;
                    break;

                default:
                    print("Oups. Something went wrong :(");
                    break;
            }

            for (int i = 0; i < Blocks.Length; i++)
            {

                //create a random vector on the ground

                Vector3 vec = new Vector3(x, y, z);

                //create an instance
                    Blocks[i] = (Transform)Instantiate(thisBlock, vec, Quaternion.identity);
                    Blocks[i].transform.parent = TContainer;
                    ready = false;
                    gameover.enabled = false;
                    StartCoroutine(Time(0.5f));

            }

        }

    }

    IEnumerator Time(float wait)
    {
        yield return new WaitForSeconds(wait);
        gameover.enabled = true;
    }


}
