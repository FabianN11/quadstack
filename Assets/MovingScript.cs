﻿using UnityEngine;
using System.Collections;

public class MovingScript : MonoBehaviour {
    private Vector3 moveDirection;
    private int d;
    private bool noMove = false;
    public float speed = 0.1f;

	// Use this for initialization
	void Start () {
        d = Random.Range(1, 5);
        switch (d)
        {
            case 1:
                moveDirection = new Vector3(-1f, 0f, 0f) * speed;
                break;

            case 2:
                moveDirection = new Vector3(0f, 1f, 0f) * speed;
                break;

            case 3:
                moveDirection = new Vector3(1f, 0f, 0f) * speed;
                break;

            case 4:
                moveDirection = new Vector3(0f, -1f, 0f) * speed;
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (!noMove)
        {
            transform.position += moveDirection;

            //control
            if ((Input.GetKeyDown(KeyCode.W)) && (d != 4))
            {
                transform.position += new Vector3(0f, 1.2f, 0f)*10 * speed;
            }

            if ((Input.GetKeyDown(KeyCode.S)) && (d != 2))
            {
                transform.position += new Vector3(0f, -1.2f, 0f)*10 * speed;
            }

            if ((Input.GetKeyDown(KeyCode.A)) && (d != 3))
            {
                transform.position += new Vector3(-1.2f, 0f, 0f)*10 * speed;
            }

            if ((Input.GetKeyDown(KeyCode.D)) && (d != 1))
            {
                transform.position += new Vector3(1.2f, 0f, 0f)*10 * speed;
            }
            //control end
        }

        if(noMove)
        {
            moveDirection = new Vector3(0f, 0f, 0f);
        }
    }

    void stop ()
    {
        SpawnScript.ready = true;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Wall")
        {
            Debug.Log("bumb");
            stop();
            noMove = true;
        }

        if (coll.tag == "Block")
        {
            stop();
            noMove = true;
        }

        if (coll.tag == "Spawn")
        {
            Application.LoadLevel(0);
        }
    }
}
